package com.aras.modir.backgroundthread.ExampleLooperThread;

import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.aras.modir.backgroundthread.R;

import static com.aras.modir.backgroundthread.ExampleLooperThread.ExampleHandler.TASK_A;
import static com.aras.modir.backgroundthread.ExampleLooperThread.ExampleHandler.TASK_B;

public class LooperExampleActivity extends AppCompatActivity {
    private static final String TAG = "LooperExampleActivity";


    private ExampleLooperThread looperThread = new ExampleLooperThread();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_looper_example);
    }

    public void StartThread(View view) {
        looperThread.start();
    }

    public void stropThread(View view) {
        looperThread.looper.quit();
    }

    public void taskA(View view) {
        Message msg = Message.obtain();
        msg.what = TASK_A;
        looperThread.handler.sendMessage(msg);

        /*Handler handler = new Handler(looperThread.looper);
//        looperThread.handler.post
        handler.post(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 5; i++) {
                    Log.d(TAG, "run: " + i);
                    SystemClock.sleep(1000);
                }
            }
        });*/
    }

    public void taskB(View view) {
        Message msg = Message.obtain();
        msg.what = TASK_B;
        looperThread.handler.sendMessage(msg);
    }
}
