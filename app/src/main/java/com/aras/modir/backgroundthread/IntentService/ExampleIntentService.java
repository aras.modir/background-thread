package com.aras.modir.backgroundthread.IntentService;

import android.app.IntentService;
import android.app.Notification;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.PowerManager;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.aras.modir.backgroundthread.R;

import static com.aras.modir.backgroundthread.ForegroundService.App.CHANNEL_ID;

public class ExampleIntentService extends IntentService {

    public static final String TAG = "Test";
    private PowerManager.WakeLock wakeLock;
    IntentServiceActivity activity = new IntentServiceActivity();
    Handler handler = new Handler();

    public ExampleIntentService() {
        super("ExampleIntentService");
        setIntentRedelivery(true);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate: ");

        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                "ExampleApp:Wakelock");
        wakeLock.acquire();
        Log.d(TAG, "Wakelock acquired");


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentTitle("Example IntentService")
                    .setContentText("Running...")
                    .setSmallIcon(R.drawable.ic_android)
                    .build();

            startForeground(1, notification);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy: ");
        wakeLock.release();
        Log.d(TAG, "Wakelock Release");
    }

    @Override
    protected void onHandleIntent(final Intent intent) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {

                        Log.d(TAG, "onHandleIntent:");
                        String input = intent.getStringExtra("inputExtra");

                        for (int i = 0; i < 10; i++) {
                            Log.d(TAG, input + " - " + i);
                            SystemClock.sleep(i);
                        }
                        Toast.makeText(getApplicationContext(), "asdasdasd", Toast.LENGTH_SHORT).show();
                    }


                });
            }

        }).start();
    }
}
