package com.aras.modir.backgroundthread.BroadcastReceiver.StaticReceiver;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.aras.modir.backgroundthread.R;

public class StaticReceiverActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_static_receiver);
    }
}
