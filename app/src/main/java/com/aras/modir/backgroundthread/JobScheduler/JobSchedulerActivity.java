package com.aras.modir.backgroundthread.JobScheduler;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.aras.modir.backgroundthread.R;

public class JobSchedulerActivity extends AppCompatActivity {
    private static final String TAG = "Test";
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_scheduler);
        this.mContext = this;

    }

    public void schedulerJob(View view) {
        ComponentName componentName = new ComponentName(this, ExampleJobservice.class);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            JobInfo jobInfo = new JobInfo.Builder(123, componentName)
                    .setPersisted(true)
                    .setPeriodic(15 * 60 * 1000)
                    .build();
            JobScheduler jobScheduler = (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);
            int resultCode = jobScheduler.schedule(jobInfo);
            if (resultCode == JobScheduler.RESULT_SUCCESS) {
                Log.d(TAG, "job Scheduled");
            } else {
                Log.d(TAG, "job Scheduling Failed");
            }

        }
    }

    public void cancelJob(View view) {
        JobScheduler jobScheduler = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            jobScheduler = (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            jobScheduler.cancel(123);
            Log.d(TAG, "Job Cancelled ");
        }
    }
}
